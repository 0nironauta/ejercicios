﻿/* CirculoVida.cs is part of Ejercicios.

    Ejercicios is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Ejercicios.  If not, see <https://www.gnu.org/licenses/>
  */
// <author>Jorge García Colmenar</author>
// <date>23/10/2018 11:46:58 AM </date>
// <summary>Clase que controla el círculo de vida de la UI</summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CirculoVida : MonoBehaviour {

    /// <summary>
    /// Primer Círculo
    /// </summary>
    Image circuloVerde;

    /// <summary>
    /// Segundo Círculo
    /// </summary>
    Image circuloAzul;

    /// <summary>
    /// Representa el círculo en el que estamos
    /// </summary>
    int nivel = 1;

    /// <summary>
    /// Esta variable está pensada para ajustar la cantidad de vida que se quiera usar.
    /// Por ejemplo, un círculo de 10 vidas necesita un factor 10.
    /// Un círculo de 100 vidas necesita factor 100.
    /// Debe ser coherente con los argumentos de las funciones de sumar y restar.
    /// </summary>
    float factor = 10.0F;

    /// <summary>
    /// Obtenemos los componentes en las variables
    /// </summary>
    private void Awake()
    {
        circuloVerde = transform.GetChild(1).GetComponent<Image>();
        circuloAzul  = transform.GetChild(0).GetComponent<Image>();
    }

    /// <summary>
    /// Aumentamos vida
    /// <paramref name="cantidad"/>la cantidad de puntos a añadir</paramref>
    /// </summary>
    public void SumarVida(int cantidad)
    {
        if (nivel == 1)
        {
            float valorActual = circuloVerde.fillAmount;
            float resultado = valorActual + cantidad / factor;
            circuloVerde.fillAmount = resultado;
        }
        else
        {
            float valorActual = circuloAzul.fillAmount;
            float resultado = valorActual + cantidad / factor;
            if (resultado > 1)
            {
                circuloVerde.fillAmount += resultado-1.0F;
                nivel = 1;
            }
            circuloAzul.fillAmount = resultado;
        }
    }

    /// <summary>
    /// Restamos vida
    /// <paramref name="cantidad"/>la cantidad de puntos a restar</paramref>
    /// </summary>
    public void RestarVida(int cantidad)
    {
        if (nivel == 1)
        {
            float valorActual = circuloVerde.fillAmount;
            float resultado = valorActual - cantidad / factor;
            if (resultado < 0)
            {
                circuloAzul.fillAmount -= Mathf.Abs(resultado);
                nivel = 0;
            }
            circuloVerde.fillAmount = resultado;
        }
        else
        {
            float valorActual = circuloAzul.fillAmount;
            float resultado = valorActual - cantidad / factor;
            if (resultado < 0)
            {
                // Muerto
                print("Muerto");
            }
            circuloAzul.fillAmount = resultado;
        }
    }
}
